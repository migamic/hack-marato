#include <iostream>
#include <vector>
#include <string>
#include <ctime>
using namespace std;

vector<string> n = {"Joan", "Marc", "Maria", "Antoni", "Pere", "Anxo", "Andrea", "Clara", "Claudia", "Irene", "Bernat", "Júlia", "Emma", "Alex", "Nil", "Leo", "Pau", "Biel", "Sofia", "Noa", "Arnau", "Ona", "Sara", "Bruno", "Miquel", "Marti", "Jaume", "Emili", "Jordi", "Mateu", "Alba", "Adam", "Lia", "Sara", "Max", "David", "Cesc", "Àngel", "Albert", "Toni", "Roger"};
vector<string> c = {"Martínez","Bonet","Pereira", "Ros", "Ramirez", "Alonso", "Garcia", "Lozano", "Puig", "Ferrer", "Lopez", "Perez", "Ruiz", "Moreno", "Navarro", "Molina", "Serra", "Soler", "Rovira", "Costa", "Font", "Pujol", "Terrades", "Grau", "Roig", "Canals", "Blanes", "Domenec", "Bosch", "Armada", "Petit", "Gomez", "Lopez"};
vector<string> m = {"Sindrome de Felty","Deficiencia LRBA","Sclerosis múltiple pediatrica","Disgenesis reticular","Sindrome TARP","Duplicació del cromosoma 10p","Eliminació del cromosoma 17p","Trisomia 13","Cromosoma 15 anellat","Duplicació del cromosoma 21q","Sindrome de Bantu","Chondrocalcinosis 2","DPM2-CDG","Malaltia de Kanzaki","Malaltia de Niemann-Pick","Sindrome Cerebrooculonasal","Microcefalia","Sindrome de Ouvrier Billson","Deficiencia prolidàsica","Malaltia de Wolman","Sindrome de Cowden","Cancer del tub falopial","Tumors del glomus jugular","Plexosarcoma","Neoplasma esplenic","Encefalitis de California","Leukemia de la cèl·lula T","Febre hemorràgica de Marburg","SARS","Poliomyelitis","Micetoma","Malaltia de Hansen","Shigelosis","Tularemia","Febre Reumàtica"};

void crea(){
    bool first = true;
    cout<<"data = '[";
    for (int i=0; i<100; ++i){
        if (not first) {
            cout << ", ";
        }
        first = false;
        int v=rand() % 35;  // Malaltia
        int malxtipus = 5;  // 5 malalties de cada tipus, ordenades
        int w=rand() % 5;  // Edat
        int x=rand() % 41; // Nom
        int y=rand() % 33; // Cognom
        long z=600000000+rand() % 100000000;
        double lon =(double)rand()/((double)RAND_MAX+1)*(2.2)+0.8;
        double lat = (double)rand()/((double)RAND_MAX+1)*(3-lon)*-1+42.5;
        cout<<"{\"malaltia\" : \""<<m[v]<<"\", \"tipus\" : \""<<v/malxtipus<<"\", \"edatId\" : \""<<w<<"\", \"tel\" : \""<<z<<"\", \"nom\" : \""<<n[x]<<" "<<c[y]<<"\", \"mail\" : \""<<n[x]<<"."<<c[y]<<"@gmail.com"<<"\", \"latitud\" : "<<lat<<", \"longitud\" : "<<lon<<"}";
    }

    cout << "]';";
}

int main(){
    srand(time(NULL));
    crea();
}
